package com.nrate.cplab2;

import com.nrate.cplab2.aircraft.AirplaneManager;

public class Main {

    public static void main(String[] args) {
        System.out.println();

        AirplaneManager airplaneManager = new AirplaneManager();
        airplaneManager.createAirplane();

        System.out.println("Airplanes:");
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Passenger planes:");
        airplaneManager.printPassengerPlanes();
        System.out.println();

        System.out.println("Cargo planes:");
        airplaneManager.printCargoPlanes();
        System.out.println();

        System.out.println("Airplanes sorted by flight range");
        airplaneManager.sortByFlightRange(SortOrder.ASCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by flight range in descending order");
        airplaneManager.sortByFlightRange(SortOrder.DESCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by model name");
        airplaneManager.sortByModelName(SortOrder.ASCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by model name in descending order");
        airplaneManager.sortByModelName(SortOrder.DESCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by model year");
        airplaneManager.sortByModelYear(SortOrder.ASCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by model year in descending order");
        airplaneManager.sortByModelYear(SortOrder.DESCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by speed");
        airplaneManager.sortBySpeed(SortOrder.ASCENDING);
        airplaneManager.printAirline();
        System.out.println();

        System.out.println("Airplanes sorted by speed in descending order");
        airplaneManager.sortBySpeed(SortOrder.DESCENDING);
        airplaneManager.printAirline();
        System.out.println();
    }


}






//        airplaneManager.printPassengerPlanes();
//        System.out.println();
//
//        airplaneManager.sortPassengerPlanesByPassengerCount(SortOrder.ASCENDING);
//        airplaneManager.printPassengerPlanes();
//
//        System.out.println();
//
//        airplaneManager.sortPassengerPlanesByPassengerCount(SortOrder.DESCENDING);
//        airplaneManager.printPassengerPlanes();
//        System.out.println();
//
//        airplaneManager.printCargoPlanes();
//        System.out.println();
//
//        airplaneManager.sortCargoPlanesByCargoCapacity(SortOrder.ASCENDING);
//        airplaneManager.printCargoPlanes();
//        System.out.println();
//
//        airplaneManager.sortCargoPlanesByCargoCapacity(SortOrder.DESCENDING);
//        airplaneManager.printCargoPlanes();
//        System.out.println();
//
//        airplaneManager.printAirline();

