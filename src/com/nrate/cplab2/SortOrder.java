package com.nrate.cplab2;

public enum SortOrder {

    ASCENDING, DESCENDING
}
