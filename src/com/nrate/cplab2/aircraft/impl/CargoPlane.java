package com.nrate.cplab2.aircraft.impl;

import com.nrate.cplab2.aircraft.Airplane;

public class CargoPlane extends Airplane {

    protected Integer cargoCapacity;

    public CargoPlane() {
    }

    public CargoPlane(Integer flightRange, String modelName, Integer modelYear, Integer speed, Integer cargoCapacity) {
        super(flightRange, modelName, modelYear, speed);
        this.cargoCapacity = cargoCapacity;
    }

    public Integer getCargoCapacity() {
        return cargoCapacity;
    }

//    public void setCargoCapacity(Integer cargoCapacity) {
//        this.cargoCapacity = cargoCapacity;
//    }

    @Override
    public String toString() {
        return "CargoPlane{" +
                "flightRange=" + flightRange +
                ", modelName='" + modelName + '\'' +
                ", modelYear=" + modelYear +
                ", speed=" + speed +
                ", cargoCapacity=" + cargoCapacity +
                '}';
    }


}
