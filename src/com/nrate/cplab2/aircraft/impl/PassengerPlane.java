package com.nrate.cplab2.aircraft.impl;

import com.nrate.cplab2.aircraft.Airplane;

public class PassengerPlane extends Airplane {

    protected Integer passengerCount;

    public PassengerPlane() {
    }

    public PassengerPlane(Integer flightRange, String modelName, Integer modelYear, Integer speed, Integer passengerCount) {
        super(flightRange, modelName, modelYear, speed);
        this.passengerCount = passengerCount;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

//    public void setPassengerCount(Integer passengerCount) {
//        this.passengerCount = passengerCount;
//    }

    @Override
    public String toString() {
        return "PassengerPlane{" +
                "flightRange=" + flightRange +
                ", modelName='" + modelName + '\'' +
                ", modelYear=" + modelYear +
                ", speed=" + speed +
                ", passengerCount=" + passengerCount +
                '}';
    }


}
