package com.nrate.cplab2.aircraft;

public abstract class Airplane {

    protected Integer flightRange;
    protected String modelName;
    protected Integer modelYear;
    protected Integer speed;

    public Airplane() {
    }

    public Airplane(Integer flightRange, String modelName, Integer modelYear, Integer speed) {
        this.flightRange = flightRange;
        this.modelName = modelName;
        this.modelYear = modelYear;
        this.speed = speed;
    }

    public Integer getModelYear() {
        return modelYear;
    }

//    public void setModelYear(Integer modelYear) {
//        this.modelYear = modelYear;
//    }

    public Integer getFlightRange() {
        return flightRange;
    }

//    public void setFlightRange(Integer flightRange) {
//        this.flightRange = flightRange;
//    }

    public String getModelName() {
        return modelName;
    }

//    public void setModelName(String modelName) {
//        this.modelName = modelName;
//    }

    public Integer getSpeed() {
        return speed;
    }

//    public void setSpeed(Integer speed) {
//        this.speed = speed;
//    }
}
