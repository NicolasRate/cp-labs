package com.nrate.cplab2.aircraft;

import com.nrate.cplab2.SortOrder;
import com.nrate.cplab2.aircraft.impl.CargoPlane;
import com.nrate.cplab2.aircraft.impl.PassengerPlane;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AirplaneManager {

    List<Airplane> airline = new ArrayList<>();

    static class FlightRangeComparator implements Comparator<Airplane> {

        @Override
        public int compare(Airplane o1, Airplane o2) {
            return Integer.compare(o1.getFlightRange(), o2.getFlightRange());
        }
    }

    class ModelNameComparator implements Comparator<Airplane> {

        @Override
        public int compare(Airplane o1, Airplane o2) {
            return o1.getModelName().compareTo(o2.getModelName());
        }
    }

    public void sortByFlightRange(SortOrder sortOrder) {
        switch (sortOrder) {
            case ASCENDING -> airline.sort(new FlightRangeComparator());
            case DESCENDING -> airline.sort(new FlightRangeComparator().reversed());
        }
    }

    public void sortByModelName(SortOrder sortOrder) {
        switch (sortOrder) {
            case ASCENDING -> airline.sort(new ModelNameComparator());
            case DESCENDING -> airline.sort(new ModelNameComparator().reversed());
        }
    }

    public void sortByModelYear(SortOrder sortOrder) {
        switch (sortOrder) {
            case ASCENDING -> airline.sort(new Comparator<Airplane>() {
                @Override
                public int compare(Airplane o1, Airplane o2) {
                    return Integer.compare(o1.getModelYear(), o2.getModelYear());
                }
            });
            case DESCENDING -> airline.sort(new Comparator<Airplane>() {
                @Override
                public int compare(Airplane o1, Airplane o2) {
                    return Integer.compare(o2.getModelYear(), o1.getModelYear());
                }
            });
        }
    }

    public void sortBySpeed(SortOrder sortOrder) {
        switch (sortOrder) {
            case ASCENDING -> airline.sort(((o1, o2) -> Integer.compare(o1.getSpeed(), o2.getSpeed())));
            case DESCENDING -> airline.sort(((o1, o2) -> Integer.compare(o2.getSpeed(), o1.getSpeed())));
        }
    }

    public void printAirline() {
        airline.forEach(System.out::println);
    }

    public void printPassengerPlanes() {
        airline.stream()
                .filter(airplane -> airplane instanceof PassengerPlane)
                .forEach(System.out::println);
    }

    public void sortPassengerPlanesByPassengerCount(SortOrder sortOrder) {
        var passengerPlaneStream = airline.stream()
                .filter(airplane -> airplane instanceof PassengerPlane)
                .map(airplane -> (PassengerPlane) airplane);

        var sortedPassengerPlaneStream = switch (sortOrder) {
            case ASCENDING -> passengerPlaneStream
                    .sorted(Comparator.comparingInt(PassengerPlane::getPassengerCount));
            case DESCENDING -> passengerPlaneStream
                    .sorted(Comparator.comparingInt(PassengerPlane::getPassengerCount).reversed());
        };

        var sortedPassengerPlaneList = sortedPassengerPlaneStream
                .collect(Collectors.toList());

        var iterator = sortedPassengerPlaneList.iterator();

        airline = airline.stream().map(airplane -> {
            if (airplane instanceof PassengerPlane && iterator.hasNext()) {
                airplane = iterator.next();
            }
            return airplane;
        }).collect(Collectors.toList());
    }

    public void sortCargoPlanesByCargoCapacity(SortOrder sortOrder) {
        var cargoPlaneStream = airline.stream()
                .filter(airplane -> airplane instanceof CargoPlane)
                .map(airplane -> (CargoPlane) airplane);

        var sortedCargoPlaneStream = switch (sortOrder) {
            case ASCENDING -> cargoPlaneStream
                    .sorted(Comparator.comparingInt(CargoPlane::getCargoCapacity));
            case DESCENDING -> cargoPlaneStream
                    .sorted(Comparator.comparingInt(CargoPlane::getCargoCapacity).reversed());
        };

        var sortedCargoPlaneList = sortedCargoPlaneStream
                .collect(Collectors.toList());

        var iterator = sortedCargoPlaneList.iterator();

        airline = airline.stream().map(airplane -> {
            if (airplane instanceof CargoPlane && iterator.hasNext()) {
                airplane = iterator.next();
            }
            return airplane;
        }).collect(Collectors.toList());
    }

    public void printCargoPlanes() {
        airline.stream()
                .filter(airplane -> airplane instanceof CargoPlane)
                .forEach(System.out::println);
    }

    public void createAirplane() {
        airline.add(new PassengerPlane(2000, "A1", 2015, 250, 130));
        airline.add(new CargoPlane(1400, "H2", 2007, 300, 500));
        airline.add(new PassengerPlane(1500, "B5", 2012, 270, 150));
        airline.add(new PassengerPlane(2500, "A3", 2017, 400, 200));
        airline.add(new PassengerPlane(1200, "C6", 2010, 240, 100));
        airline.add(new CargoPlane(1000, "F5", 2014, 320, 300));
        airline.add(new PassengerPlane(1300, "C3", 2012, 270, 150));
        airline.add(new PassengerPlane(1400, "D1", 2016, 360, 110));
        airline.add(new CargoPlane(1750, "H2", 2007, 310, 400));
        airline.add(new CargoPlane(2000, "G3", 2018, 380, 250));
        airline.add(new PassengerPlane(750, "B2", 2011, 230, 300));
    }

}
